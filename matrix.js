function validate_matrix(matrix1, matrix2) {
	var matrix1_column_length = matrix1[0].length;
	var matrix2_row_length = matrix2.length;
	if (matrix1_column_length == matrix2_row_length) {
		return true;
	}
	else {
		return false;
	}
}



function multiplymatrix(matrix1, matrix2) {
	var result = [], sum = 0;

	// matrix1 row_length1 column_length1 matrix2 row_length2 column_length2

	// matrix1_row_index matrix2_column_index

	var matrix1_row_length1 = matrix1.length;
	var matrix1_column_length1 = matrix1[0].length;
	var matrix2_row_length2 = matrix2.length;
	var matrix2_column_length2 = matrix2[0].length;

	if (validate_matrix(matrix1, matrix2)) {
		for (var matrix1_row_index = 0; matrix1_row_index < matrix1_row_length1; matrix1_row_index++) {
			result[matrix1_row_index] = []
			for (var matrix_column_index = 0; matrix_column_index < matrix2_column_length2; matrix_column_index++) {
				sum = 0;
				for (var k = 0; k < matrix1_column_length1; k++) {
					sum += (matrix1[matrix1_row_index][k]) * (matrix2[k][matrix_column_index]);
				}
				result[matrix1_row_index][matrix_column_index] = sum;
			}
		}
		return result;
	}
	else {
		return "not compatible"
	}


}
let m1 = [
	[2, 3],
	[4, 5],

];
let m2 = [
	[5, 6],
	[7, 8],
];
var output = multiplymatrix(m1, m2);
console.log(output)